# Changelog

All notable changes to `HMW starter theme` will be documented in this file

## 1.2.17

- Added hooks / filters into the footer.php file

## 1.2.11

- Make sure google fonts don't enqueue by the plugin if not enabled

## 1.2.8

- Make social icons open in a new window by default

## 1.0.0 - 201X-XX-XX

- initial release
