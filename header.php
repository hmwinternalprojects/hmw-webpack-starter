<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

 /* To change this icon add_filter inside functions.php */
 $mobile_toggle = apply_filters( 'mobile-toggle-icon', '<button class="menu-toggle" id="menu-toggle" aria-expanded="false"><span class="screen-reader-text">Menu</span>
          <svg class="icon icon-menu-toggle" aria-hidden="true" viewBox="0 0 100 80" width="40" height="40">
            <rect class="line line-1" width="100" height="10"></rect>
            <rect class="line line-2" y="30" width="100" height="10"></rect>
            <rect class="line line-3" y="60" width="100" height="10"></rect>
          </svg>
        </button>' );

 if (function_exists('get_field')) :
  $always_show_mobile_nav = get_field('always_show_mobile_nav', 'option') ?: null;
  if (isset($always_show_mobile_nav)) {
    $builderClass = 'always_show_mobile_nav';
  } else {
    $builderClass = '';
  }
 endif;

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="https://gmpg.org/xfn/11">

  <?php wp_head(); ?>
</head>

<?php 
	if (function_exists('get_field')) {
		if (get_field('BMCB_use_PageBuilder', get_the_ID())) {
			$builderClass = 'page-builder-enabled';
		}
	}
?>


<body <?php body_class([$builderClass, 'no-js']); ?>>
<?php do_action('after_body_open_tag'); ?>
<?php 
  if (function_exists('get_field') && !is_admin() && get_field('sitewide_message_enabled', 'option') && !isset($_COOKIE["sitewide_message"])) { 
    echo apply_shortcodes('[sitewide-message]'); 
  }   
?>
<?php if (isset($always_show_mobile_nav)) : ?>
  <div class="mobile-menu">
    <div id="main-nav" class="always_show_mobile_nav">
      <nav id="top-menu-nav" class="lg:flex items-center">
        <?php
          echo wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '', 'fallback_cb' => '', 'menu_class' => 'main-menu-nav nav-menu', 'menu_id' => 'main-menu', 'echo' => false ) );
        ?>
        <?php 
          if (function_exists('hmw_woocommerce_header_cart')) : 
            hmw_woocommerce_header_cart(); 
          endif; 
        ?>
      </nav>
    </div> <!-- #main-nav -->
  </div>
<?php endif; ?>
<?php wp_body_open(); ?>
  
  <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'hmw' ); ?></a>
  <div id="page" class="site">
    <?php if (get_field('enable_top_header', 'option')) : ?>
    <div id="top-header">
      <div class="container">
        <div id="secondary-menu" class="nav-menu--horizontal">
          <?php
						echo wp_nav_menu( array( 'theme_location' => 'top-menu', 'container' => '', 'fallback_cb' => '', 'menu_class' => 'top-header__menu nav-menu nav-menu--horizontal', 'menu_id' => 'top-menu', 'echo' => false ) );
					?>
        </div> <!-- #secondary-menu -->
        <?php if (get_field('enable_top_header_phone', 'option')) : ?>
        <div class="header-phone-wrapper flex items-center">
          <i class="fas text-link fa-mobile-alt pr-1"></i>
          <?php echo do_shortcode( '[company-phone]' ); ?>
        </div>
        <?php endif; ?>
      </div> <!-- .comtainer -->
    </div> <!-- #top-header -->
    <?php endif; ?>

    <?php 
    if (get_field('main_header_background_colour', 'option')) : 
      $header_colour = get_field('main_header_background_colour', 'option');
    endif;
  ?>
    <header id="main-header"
      <?php echo (isset($header_colour) && $header_colour !== 'None') ? "class='bg-{$header_colour}'" : null;  ?>>
      <div class="container flex items-center clearfix">
        <div class="logo_container">
          <?php echo do_shortcode( '[logo]' ); ?>
        </div>        
        
        <?php do_action('main-header:before-nav'); ?>        
       
        
        
        <?php if (!$always_show_mobile_nav) : ?>
          <div id="main-nav">
            <nav id="top-menu-nav" class="lg:flex items-center">
              <?php
                echo wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '', 'fallback_cb' => '', 'menu_class' => 'main-menu-nav nav-menu nav-menu--horizontal', 'menu_id' => 'main-menu', 'echo' => false ) );
              ?>
              <?php 
                if (function_exists('hmw_woocommerce_header_cart')) : 
                  hmw_woocommerce_header_cart(); 
                endif; 
              ?>
            </nav>
          </div> <!-- #main-nav -->
        <?php endif; ?>

        <?php do_action('main-header:after-nav'); ?>

        <?= $mobile_toggle; ?>

      </div> <!-- .container -->
    </header> <!-- #main-header -->
