<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package _s
 */

get_header();
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">

    <?php if ( have_posts() ) : ?>

    <div class="bmcb-section container">
      <div class="bmcb-row row">
        <div class="bmcb-column col-12">


          <header class="bmcb-row text-center pt-0">
            <?php
				the_archive_title( '<h1 class="page-title">', '</h1>' );
				the_archive_description( '<div class="archive-description">', '</div>' );
				?>
          </header>

          <?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;

			the_posts_navigation();

		else :

      get_template_part( 'template-parts/content', 'none' ); ?>


        </div>
      </div>
    </div>

    <?php endif;
		?>

  </main><!-- #main -->
</div><!-- #primary -->

<?php
if (function_exists('render_common_globals')) {
  echo render_common_globals();
}
get_footer();
