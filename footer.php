<?php
/**
 * Fires after the main content, before the footer is output.
 *
 * @since 3.10
 */
?>
<?php do_action('before_site_footer'); ?>

<footer id="colophon" class="site-footer">
  <?php do_action('before_site_footer_global'); ?>
  <?php 
			global $buildy;
      $globalFooterID = apply_filters( 'global_footer_ID', '58');
			echo $buildy->renderFrontend($globalFooterID); 
		?>
  <?php do_action('after_site_footer_global'); ?>
</footer><!-- #colophon -->

<?php do_action('after_site_footer'); ?>
</div><!-- #page -->


<?php wp_footer(); ?>

</body>

</html>
