<?php
 get_header();
?>

<div <?php post_class(); ?> id="main-content">

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="content-wrap">

			<?php
				the_content();
			?>

		</div>

	<?php endwhile; ?>
</div> <!-- #main-content -->

<?php

get_footer();
