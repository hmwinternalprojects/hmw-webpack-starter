<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package _s
 */

get_header();
?>

<?php do_action('before_search_content'); ?>
<section id="primary" class="content-area">
  <main id="main" class="site-main">
    <?php 
      do_action('before_search_results');
      if ( have_posts() ) : ?>
    <?php
        
        /* Start the Loop */
        while ( have_posts() ) :
          the_post();

          /**
           * Run the loop for the search to output the results.
           * If you want to overload this in a child theme then include a file
           * called content-search.php and that will be used instead.
           */
          get_template_part( 'template-parts/content', 'search' );

        endwhile;

        the_posts_navigation();

      else :

        // get_template_part( 'template-parts/content', 'none' );

      endif;
      do_action('after_search_results');
      ?>

  </main><!-- #main -->
</section><!-- #primary -->
<?php do_action('after_search_content'); ?>

<?php
get_footer();
