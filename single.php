<?php
 get_header();
?>

<div <?php post_class(); ?> id="main-content">

	<div class="bmcb-section container xs:pb-0">
		<div class="bmcb-row row xs:pb-0">
			<div class="bmcb-column col-xs-12 col-sm-12 col-md-8 col-lg-8 ">
				<div class="post-meta post-meta__date">
					<?php echo get_the_date(); ?>
				</div>
				<div class="bmcb-code-module bmcb-module  ">
					<h1><?php echo do_shortcode('[echo-field field="title"]'); ?></h1>    
				</div>
				<div class="post-categories">
					<?php $categories = get_categories(); ?>
					<ul class="post-categories__list">
						<?php foreach($categories as $term) : ?>
							<?php if($term->name !== 'Uncategorized') : ?>
								<li class="post-categories__list-item">
									<a class="post-categories__link" href="<?php echo get_category_link($term); ?>">
										<?php echo $term->name; ?>
									</a>
								</li>
							<?php endif; ?>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
			<div class="bmcb-column col-xs-12 col-sm-12 col-md-4 col-lg-4 ">
				<div class="bmcb-code-module bmcb-module">
					<?php echo do_shortcode('[page-content-links]'); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="bmcb-section container-fluid">
		<div class="bmcb-row row">
			<div class="bmcb-column col-xs-12">
				<?php the_post_thumbnail('full-width'); ?>
			</div>
		</div>
	</div>

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="content-wrap">

			<div class="bmcb-section container ">
				<div class="bmcb-row row ">
					<div class="bmcb-column col-xs-12 col-sm-12 col-md-8 col-lg-8 ">
						<div class="bmcb-text-module bmcb-module entry-content">
							<?php
								$ID = get_the_ID();
								$intro = get_field('introduction', $ID) ? get_field('introduction', $ID) : false;

							if ($intro) {
								echo "<p id='post-intro' class='font-large'>$intro</p>";
							} ?>
							<?php the_content(); ?>   
						</div>
					</div>
					<div class="bmcb-column col-xs-12 col-sm-12 col-md-4 col-lg-4 sidebar">
						<?php get_sidebar(); ?>
					</div>
				</div>
			</div>

		</div>

	<?php endwhile; ?>

	<div class="bmcb-section container ">
		<hr style="height: 4px;" class="bg-lightgray xs:my-1" />
		<div class="bmcb-row row ">
			<div class="bmcb-column col-xs-12">
				<div class="bmcb-code-module bmcb-module">
					<h2>Related stories</h2>
					<?php
						$categories = get_the_category( get_the_ID() );
						echo do_shortcode('[universal-grid cat="'. $categories[0]->term_id .'" perpage="3"]');
					 ?>
				</div>
			</div>
		</div>
	</div>

</div> <!-- #main-content -->

<?php

get_footer();
