<article  id="post-<?php the_ID(); ?>" <?php post_class("article-grid__article"); ?>>
	<header class="article-grid__header">
		<?php the_title( '<h2 class="article-grid__title entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );?>
    <div class="article-grid__image-wrapper">
      <?php hmw_post_thumbnail('article-grid'); ?>
    </div>
	</header>
	<div class="article-grid__body entry-content">
		<?php

      // Most of the time
      the_excerpt();
		
    // the_content( sprintf(
		// 	wp_kses(
		// 		/* translators: %s: Name of current post. Only visible to screen readers */
		// 		__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'hmw' ),
		// 		array(
		// 			'span' => array(
		// 				'class' => array(),
		// 			),
		// 		)
		// 	),
		// 	get_the_title()
		// ) );

		?>
	</div><!-- .entry-content -->

	<footer class="article-grid__footer entry-footer">
		<?php hmw_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
