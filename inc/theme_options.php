<?php if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Site Options',
		'menu_title'	=> 'Site Options',
		'menu_slug' 	=> 'site-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

// Generate branding css
function save_theme_css_settings() {
  $screen = get_current_screen();
	if (strpos($screen->id, "site-options") == true || strpos($screen->id, "bmcb-settings") == true) {
    $css_dir = get_stylesheet_directory();
    $template_dir = get_template_directory();

    ob_start(); // Capture all output into buffer

    require($template_dir . '/inc/generate-theme-options-css.php'); // Grab the custom style php file

    $css = ob_get_clean(); // Store output in a variable, then flush the buffer

    file_put_contents($css_dir . '/public/hmw-theme-options.css', $css, LOCK_EX); // Save it as a css file
	}
}

add_action('acf/save_post', 'save_theme_css_settings', 20);
