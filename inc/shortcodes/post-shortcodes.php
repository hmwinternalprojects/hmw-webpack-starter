<?php

// [testi-slider] You can use any JS slider library you like. Default is Siema
if (!function_exists('testi_slider_shortcode')) {
  function testi_slider_shortcode($atts) {
    $atts = shortcode_atts( 
          array(
        'perpage' => 2,
        'image' => true,
        'class' => ''
          ), 
          $atts);
  
    $args = array(
          'post_type' => 'testimonials',
          'posts_per_page' => $atts['perpage'],
          'post_status' => 'publish'
      );
  
    $query = new WP_Query($args);
  
    ob_start();
    
    ?>

<div class="my-slider testimonial-slider">
  <?php if ($query->have_posts()) : ?>
  <?php while($query->have_posts()) : $query->the_post(); 
          $ID = get_the_id();
          $class = $atts['class'];
          $imageEnabled = $atts['image'] !== 'false';
          $image = get_the_post_thumbnail_url($ID,'full'); // Returns URL of any custom size (obv)
        ?>
  <div
    class="testimonial-slider__slide <?php echo ($class) ? $class : '' ?> <?php echo !$imageEnabled ? 'image-false' : 'image-true'; ?>"
    <?php echo $imageEnabled ? "style=\"background: url('$image')\"" : ''; ?>>
    <div class="container">
      <div class="testimonial-slider__content-box">
        <div class="testimonial-slider__content">
          <div class="testimonial-slider__body"><?php echo __(wp_trim_words(get_the_content(), 40)); ?></div>
          <div class="testimonial-slider__info"><?php echo __("Samantha Smith"); ?></div>
        </div>
        <div class="testimonial-slider__arrow-group">
          <div class="testimonial-slider__arrow arrow-left">
            <i class="fa fa-chevron-left"></i>
          </div>
          <div class="testimonial-slider__arrow arrow-right">
            <i class="fa fa-chevron-right"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php endwhile; ?>
  <?php endif; ?>
</div>

<?php
    $output = ob_get_clean();
      wp_reset_postdata();
      return $output;
  }
  add_shortcode('testi-slider', 'testi_slider_shortcode');
}

// [list-posts] This is a universal shortcode to display any post/posttype
// In a css grid format with pagination and a basic category filter
if (!function_exists('list_posts_shortcode')) {
  function list_posts_shortcode($atts, $request) {
	
	if (get_query_var('categories')) {
		$categoryIDs = get_query_var('categories');
	}

	$atts = shortcode_atts( [
		"perpage" => 6,
		"title" => null,
		"offset" => 0,
		"cols" => 3,
		"cats" => null,
		"exclude_cats" => null,
		"post_type" => 'post',
		"paged" => true,
		"orderby" => "menu_order",
		"order" => "ASC",
    "featured_key" => false,
    "pagination_enabled" => false,
    "pagination_type" => 'default',
    "pagination_trigger" => 'click',
    "content_template" => null,
    "empty_message" => false    
	], $atts );
	
	if (!get_query_var('categories') && isset($atts['cats'])) {
		$categoryIDs = $atts['cats']; 
	}

	$pagination_enabled = ($atts['pagination_enabled'] && $atts['perpage'] !== -1);	
	$current_page = get_query_var('paged');
	$current_page = max( 1, $current_page );

	$offset_start = $atts['offset'];
	$offset = ( $current_page - 1 ) * $atts['perpage'] + $offset_start;
	
	$args = [
		"posts_per_page" => $atts['perpage'],
		"offset" => $offset,
		"post_type" => $atts['post_type'],
		"post_status" => "publish",
    "orderby" => $atts['orderby'],
    "order" => $atts['order'],
    
	];

  if (isset($categoryIDs)) {
    $args["category__in"] = comma_list_to_int_arr($categoryIDs);
	}
  
  if (isset($atts['exclude_cats'])) : 
    $args['post__not_in'] = comma_list_to_int_arr($atts['exclude_cats']);
  endif; 
  
	if ($pagination_enabled) {
		$args['paged'] = $current_page;
	}

  if (!empty($atts['featured_key'])) {
		$args['meta_query'] = [
      [
        'key' => $atts['featured_key'],
        'value' => '1',
        'compare' => '==' // not really needed, this is the default
      ]
      ];
	}

  if (array_key_exists('category__in', $args) && count($args['category__in']) === 1) {
    $category = get_category( $categoryIDs );
    $category_name = strtolower($category->name);
  }
	
	$query = new WP_Query($args); 
  $queryVars = json_encode($query->query_vars, true);
	$total_rows = max( 0, $query->found_posts - $offset_start );
	$total_pages = ceil( $total_rows / $atts['perpage'] );

  $pagination = ($pagination_enabled && $total_pages > 1);

  $template_name = !empty($atts['content_template']) ? $atts['content_template'] : (isset($category_name) ? "{$atts['post_type']}-{$category_name}" : $atts['post_type']);
  $template = locate_template( "template-parts/content-{$template_name}.php");

	ob_start();
	if ($query->have_posts()) : ?>

<div class="article-grid__wrapper">
  <?php if (isset($atts['title'])) : ?>
    <h2 class="article-grid__title"><?= $atts['title'] ?></h2>
  <?php endif; ?>
  <div class="article-grid article-grid__<?= $template_name ?> grid grid-lg-<?php echo $atts['cols'];?>">
    <?php while ($query->have_posts()) : 
          $query->the_post(); 
          $ID = get_the_ID();
          $index = $query->current_post; 
          $postURL = get_permalink(); 
          ?>

    <?php 
    if ($template):
      // Include like this to get access to variables in this scope
      include($template); 
    else: 
      get_template_part('template-parts/content');
    endif;
    ?>

    <?php endwhile; ?>
    
  </div>
  <?php if ($pagination && $atts['pagination_type'] === 'loadmore') : ?>
      <div class="loadmore-button btn btn__loader"
        data-queryvars='<?php echo $queryVars ?>'
        data-currentpage="<?php echo $current_page; ?>" 
        data-offset="<?php echo $offset_start; ?>" 
        data-totalpages="<?php echo $total_pages; ?>"
        data-perpage="<?php echo $atts['perpage']; ?>"
        data-container=".article-grid"
        data-trigger="<?php echo $atts['pagination_trigger']; ?>"
        data-templatepart="<?= $template_name; ?>">
        Load more
      </div>
      <svg class="modal-loader spinner" viewBox="0 0 50 50"><circle class="path" cx="25" cy="25" r="20" fill="none" stroke-width="5"></circle></svg>

    <?php endif; ?>
</div>

  

<?php if ($pagination && $atts['pagination_type'] === 'default') : ?>
  <div class="pagination">
    <?php echo paginate_links( array(
            'total'   => $total_pages,
            'current' => $current_page,
            'prev_text' => 'Prev',
            'next_text' => 'Next'
          ) ); ?>
  </div>
<?php endif; ?>

<?php else : ?>

  <?php 
    // If you set this to true the "no posts found" h2 will be returned and displayed.
    if ($atts['empty_message']) : 
    ?>
    <h2><?= __('No posts found', 'heritage-medical') ?></h2>
    <?php 
      // Otherwise you can do a check if any posts were found, and do an if/else statement based on this shortcode (really useful for things like related posts)
      else :
        wp_reset_query(); wp_reset_postdata();
        return false;
      endif; 
    ?>

<?php 
	endif;
    wp_reset_query(); wp_reset_postdata();
	return ob_get_clean();
}
  add_shortcode('list-posts', 'list_posts_shortcode');
}

// Echo either Yogi Bear or Boo Boo, Yogi: [echo-field field="content"] | Boo Boo: [echo-field acf_field="anything"]
// More Yogi [echo-field field="post_thumbnail" size="thumbnail"] // params are useful for image sizes
if (!function_exists('echo_field_shortcode')) {
  function echo_field_shortcode($atts) {
      if (is_admin()) {return null;}
  
      extract(shortcode_atts(array(
          'field' => '',
          'acf_field' => '',
          'params' => ''
      ), $atts));
  
      $ID = get_the_ID();
  
      $output = false;
  
      if (!empty($acf_field)) {
          // Check to ensure boo boo is available
          if (function_exists('get_field')) {
              $acf_field = trim(htmlspecialchars_decode($acf_field),'\'"');
              $output = get_field($acf_field, $ID);
          }
      }
  
      // Yogi takes presidence -- make sure field is not set if acf_field is
      if (!empty($field)) {
        
        if ($field === 'thumbnail') {
          $field = "post_{$field}";
        }
        
        $output = "get_the_{$field}"; 
      } 
  
      // There are inconsistencies with the amount of params in each wordpress function so here is where you adjust 
      // as needed on a case by case... E.G ID needs to come first when doing custom image sizes, but not others
      if ($output && function_exists($output)) {
          switch($output) {
              case 'get_the_post_thumbnail':
                  $output = $output($ID, $params);
              break;
              case 'get_the_ID':
                  $output = $ID;
              break; 
              default:
                  $output = $output();
          }
      }
  
      return $output ?: "There were no fields matching your query {$atts['acf_field']}";
  }
  add_shortcode('echo-field', 'echo_field_shortcode');
}

// [related-posts cat="" offset="" post_type=""] This is a universal shortcode to display any post/posttype
// In a css grid format with pagination and a basic category filter
if (!function_exists('related_posts_shortcode')) {
  function related_posts_shortcode($atts, $request) {
    
    if (get_query_var('categories')) {
      $categoryIDs = get_query_var('categories');
    }
  
    $atts = shortcode_atts( [
      "perpage" => 6,
      "offset" => 0,
      "cols" => 3,
      "gap" => 0,
      "cat" => null,
      "post_type" => 'post',
      "paged" => true,
      "text" => 'light'
    ], $atts );
    
    if (!get_query_var('categories') && isset($atts['cat'])) {
      $categoryIDs = $atts['cat']; 
    }
    
    $args = [
      "posts_per_page" => $atts['perpage'],
      "post_type" => $atts['post_type'],
      "post_status" => "publish",
      "post__not_in" => array(get_the_ID()),
      "category__in" => $categoryIDs ? [$categoryIDs] : '',
      'post__not_in' => array(get_the_ID())
    ];
    
    // Headers white or black
    $color = $atts['text'] === "light" ? 'white' : 'black';
    $opposite_color = $atts['text'] !== "light" ? 'black' : 'white';
  
    
    $query = new WP_Query($args); 
  
    ob_start();
    if ($query->have_posts()) : ?>


<div class="section container">
  <div class="article-grid article-grid__related-posts grid">
    <?php while ($query->have_posts()) :       
          $query->the_post();
          $index = $query->current_post; 
          ?>

    <div class="col-<?php echo 12 / intval($atts['cols']);?>">
      <?php include(locate_template("template-parts/article-condensed.php")); ?>
    </div>

    <?php endwhile; ?>

  </div>
</div>

<?php 
    endif;
    wp_reset_postdata();
    return ob_get_clean();
  }
  add_shortcode('related-posts', 'related_posts_shortcode');
}
