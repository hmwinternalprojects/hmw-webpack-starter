<?php

// [menu name=""]
if (!function_exists('menu_shortcode')) {
  function menu_shortcode($atts, $content = null) {
    $atts = shortcode_atts([ 
      'name' => null, 
      'depth' => 0
    ], $atts);
    $content .= wp_nav_menu( array( 'menu' => $atts['name'], 'echo' => false, 'depth' => $atts['depth'] ) );
    return $content;
  }
  add_shortcode('menu', 'menu_shortcode');
}

// [logo type="header / footer"]
// Header is set in customizer / Footer is set under site options "Company Logo Alt"
// Will show an image set in the customizer tab --- Or it will display the site name
if (!function_exists('logo_shortcode')) {
  function logo_shortcode($atts) {
  if ( is_admin() ){ return null; }

  $atts = shortcode_atts( [
		"type" => 'header',
	], $atts );

  if ( function_exists( 'the_custom_logo' )) {
    
    // From theme customizer -- Vertical Logo
    if ($atts["type"] === 'header') : 
      $custom_logo_id = get_theme_mod( 'custom_logo' );
    endif;

    if ($atts["type"] === 'footer' && function_exists('get_field')) :
      $custom_logo_id = get_field( 'company_logo_alt', 'option' );
    endif;
        
    ob_start(); ?>
<a href="<?= get_home_url() ?>">
  <?php if ($custom_logo_id) : ?>
  <?php echo wp_get_attachment_image( $custom_logo_id , 'full', '', ["class" => "logo__{$atts['type']}"] ); ?>
  <?php else : ?>
  <h2 class="site-logo-text mb-0"><?php echo do_shortcode( '[sitename]' ); ?></h2>
  <?php endif; ?>
</a>
<?php 
      return ob_get_clean();
    }
    
  }
  add_shortcode('logo', 'logo_shortcode');
}

//[searchform]
if (!function_exists('hmw_display_search_form')) {
  function hmw_display_search_form() {
    return get_search_form(false);
  }
  add_shortcode('searchform', 'hmw_display_search_form');
}

// [company-phone]
if (!function_exists('company_phone_shortcode')) {
  function company_phone_shortcode($atts) {

    $atts = shortcode_atts([
      'text' => null,
      'icon' => false,
      'field' => 'company_phone',
      'type' => 'tel'
    ],$atts);

    if ( is_admin() ){ return null; }
    $company_phone = 'xxxx xxx xxx';
    if (function_exists('get_field')) {
      $company_phone = get_field($atts['field'], 'option') ? get_field($atts['field'], 'option') : $company_phone;
    } 
	  
	if (!$company_phone) {return;}
    
	  ob_start(); 
    
    ?>

<?php  ?>

<a class="<?= $atts['field']; ?>" href="<?= format_phone($company_phone, "61", false, $atts['type']) ?>">
    <?php if ($atts['icon']) : ?>
      <i class="<?= esc_attr($atts['icon']) ?>"></i>
    <?php endif; ?>
  <span><?php esc_attr_e( $atts['text'] ?: $company_phone, 'hmw-starter-child' ); ?></span>
</a>

<?php 
    return ob_get_clean();
  }
  add_shortcode('company-phone', 'company_phone_shortcode');
}

// [company-fax]
if (!function_exists('company_fax_shortcode')) {
  function company_fax_shortcode() {
  if ( is_admin() ){ return null; }

  ob_start();

    echo apply_shortcodes("[company-phone type='fax' field='company_fax']");

  return ob_get_clean();
  }
add_shortcode('company-fax', 'company_fax_shortcode');
}


// [email]
if (!function_exists('company_email_shortcode')) {    
  function company_email_shortcode($atts) {

    $atts = shortcode_atts([
      'text' => null,
      'icon' => false
    ],$atts);

      if ( is_admin() || !function_exists('get_field') ){ return null; }
  
      $company_email = get_field('company_email', 'option') ? get_field('company_email', 'option') : '';
  
      ob_start(); ?>

<a class="company-email" href="mailto:<?php echo esc_attr( $company_email ); ?>">
  <?php if ($atts['icon']) : ?>
    <i class="<?= esc_attr($atts['icon']) ?>"></i>
  <?php endif; ?>
  <span><?php echo esc_attr_e( $atts['text'] ?: $company_email, 'hmw-starter-child' ); ?></span>
</a>
<?php 
      return ob_get_clean();
  }
  add_shortcode('company-email', 'company_email_shortcode');
}


// [year]
if (!function_exists('year_shortcode')) {
  function year_shortcode() {
    if ( is_admin() ){ return null; }
    return date('Y');
  }
  add_shortcode('year', 'year_shortcode');
}

// [sitename]
if (!function_exists('sitename_shortcode')) {
  function sitename_shortcode() {
      if ( is_admin() ){ return null; }
      return get_option( 'blogname' );
  }
  add_shortcode('sitename', 'sitename_shortcode');
}

// [description]
add_shortcode('description', function() {
  if ( is_admin() ){ return null; }
  return get_bloginfo( 'description' );
});

// [home_url]
if (!function_exists('home_url_shortcode')) {
  function home_url_shortcode() {
      if ( is_admin() ){ return null; }
      return esc_url( home_url( '/' ) );
  }
  add_shortcode('home_url', 'home_url_shortcode');
}

// [credits]
if (!function_exists('credits_shortcode')) {
  function credits_shortcode($atts, $content = null) {
      if ( is_admin() ){ return null; }
  
      extract(shortcode_atts(array( 'logo' => false, ), $atts));
      $domain_name = preg_replace('/^www\./','',$_SERVER['SERVER_NAME']);
      if( $logo == 'true' ) {
          $html = '<div class="hmw-credit-link">Website by <a href="https://www.handmadeweb.com.au/?utm_source=client_footer&utm_medium=referral&utm_campaign='. $domain_name .'" rel="nofollow" target="_blank"><img src="'. get_stylesheet_directory_uri() .'/images/hmw-logo-white.png" alt="Handmade Web & Design" /></a></div>';
      } else {
        $html = '<div class="hmw-credit-link">Website by <a href="https://www.handmadeweb.com.au/?utm_source=client_footer&utm_medium=referral&utm_campaign='. $domain_name .'" rel="nofollow" target="_blank">Handmade Web & Design</a></div>';
      }
      return $html;
  }
  add_shortcode('credits', 'credits_shortcode');
}

// Font awesome social icons for the footer
if (!function_exists('social_icons_shortcode')) {
  function social_icons_shortcode() {   
    
    ob_start();    
    
    if( have_rows('social_links', 'option') ): ?>

<ul class="fa-social-icons flex items-center mb-0">

  <?php while( have_rows('social_links', 'option') ): the_row(); ?>

  <li class="fa-social-icon flex items-center px-1"><a href="<?php the_sub_field('link'); ?>" target="_blank"
      class="text-lg fa fab fa-<?php echo strtolower(get_sub_field('network')); ?>"></a></li>

  <?php endwhile; ?>

</ul>

<?php endif;
      
    return ob_get_clean();
  
  }
  add_shortcode('social-icons', 'social_icons_shortcode');
}

// [share_post]
if (!function_exists('share_post_shortcode')) {
  function share_post_shortcode($atts) {
      extract(shortcode_atts(array(
      'title' => 'Share'
      ), $atts));
      ob_start();
      if( is_single() || function_exists('is_product') && is_product() ) {
      $img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()),'full'); ?>
<div class="social-share">
  <span class="share-title"><?php echo $title; ?></span>
  <ul>
    <li class="facebook fab fa-facebook"><a
        href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" target="_blank"></a></li>
    <li class="twitter fab fa-twitter"><a href="https://twitter.com/home?status=<?php echo get_permalink(); ?>"
        target="_blank"></a></li>
    <li class="pinterest fab fa-pinterest"><a
        href="https://pinterest.com/pin/create/button/?url=&media=<?php echo $img[0]; ?>&description="
        target="_blank"></a></li>
    <li class="email fas fa-envelope"><a href="mailto:?&body=<?php echo get_permalink(); ?>"></a></li>
  </ul>
</div>
<?php }
      return ob_get_clean();
  }
  add_shortcode('share_post', 'share_post_shortcode');
}
