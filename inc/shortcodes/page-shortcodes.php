<?php 

// [list-child-pages]
if (!function_exists('list_child_pages_shortcode')) {
  function list_child_pages_shortcode() {
      if(is_admin()){ return null; }
  
      $ID = get_the_ID();
  
      $args = array(
          'post_parent' => $ID,
          'numberposts' => -1,
          'post_status' => 'publish',
          'orderby' => 'menu_order',
          'order' => 'ASC'
      );
  
      $children = get_children( $args );
  
      if (get_field('additional_icon_blocks', $ID)) {
          foreach(get_field('additional_icon_blocks') as $icon_block) {
              array_push($children, $icon_block);
          }
      }
      
      ob_start();    
      
      ?>

<ul class="child-page-list">

  <?php if ($children) : foreach($children as $child) : ?>

  <li class="child-page hover-grow-sm">
    <div class="child-page__top">
      <?php the_post_thumnail(); ?>
      <?php if (get_field('page_icon', $child->ID)) : ?>
      <i
        class="icon-before icon-<?php echo get_field('page_icon', $child->ID) ? get_field('page_icon', $child->ID) : 'fa-marker'; ?>"></i>
      <?php endif; ?>
    </div>
    <div class="child-page__body">
      <a href=""><?php echo $child->post_title; ?></span></a>
    </div>
  </li>

  <?php endforeach; endif; ?>

</ul>

<?php
      
      $output = ob_get_clean();
      
      return $output;
  }
  add_shortcode('list-child-pages', 'list_child_pages_shortcode');
}


// For the buildy feature "Internal Link"
if (!function_exists('page_content_links_shortcode')) {
  function page_content_links_shortcode() {
    return '<div id="page-content-links"></div>';
  }
  add_shortcode('page-content-links', 'page_content_links_shortcode');
}

add_shortcode('sitewide-message', function($atts) {
	if ( is_admin() || !get_field('sitewide_message_enabled', 'option')) {return null;}

  $atts = shortcode_atts( [
    "icon" => 'fas fa-times'
  ], $atts);

	$bg_colour = get_field('sitewide_message_bg_colour', 'option') ?? '';
	$text_colour = get_field('sitewide_message_text_colour', 'option') ?? '';

	ob_start(); 
	?>

	<?php if (get_field('sitewide_message_content', 'option')) { ?>
			<div id="sitewide-message-bar" role="dialog" aria-describedby="dialog-description" class="sitewide-message text-light light" style="background: <?= $bg_colour; ?>; color: <?= $text_colour; ?>;">
				<div id="dialog-description" class="sitewide-message__content">
					<?php echo get_field('sitewide_message_content', 'option'); ?>
				</div> 
				<i tabindex="6" class="<?= $atts['icon']; ?> bar-exit"></i>
			</div> 
	<?php } ?>

	<?php

	return ob_get_clean();
});
