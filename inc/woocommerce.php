<?php
/**
 * WooCommerce Compatibility File
 *
 * @link https://woocommerce.com/
 *
 * @package _s
 */

/**
 * WooCommerce setup function.
 *
 * @link https://docs.woocommerce.com/document/third-party-custom-theme-compatibility/
 * @link https://github.com/woocommerce/woocommerce/wiki/Enabling-product-gallery-features-(zoom,-swipe,-lightbox)-in-3.0.0
 *
 * @return void
 */
function hmw_woocommerce_setup() {
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'hmw_woocommerce_setup' );

/**
 * WooCommerce specific scripts & stylesheets.
 *
 * @return void
 */
function hmw_woocommerce_scripts() {
	// wp_enqueue_style( 'hmw-woocommerce-style', get_template_directory_uri() . '/woocommerce.css' );

	$font_path   = WC()->plugin_url() . '/assets/fonts/';
	$inline_font = '@font-face {
			font-family: "star";
			src: url("' . $font_path . 'star.eot");
			src: url("' . $font_path . 'star.eot?#iefix") format("embedded-opentype"),
				url("' . $font_path . 'star.woff") format("woff"),
				url("' . $font_path . 'star.ttf") format("truetype"),
				url("' . $font_path . 'star.svg#star") format("svg");
			font-weight: normal;
			font-style: normal;
		}';

	wp_add_inline_style( 'hmw-child-frontend-styles', $inline_font );
}
add_action( 'wp_enqueue_scripts', 'hmw_woocommerce_scripts' );

/**
 * Disable the default WooCommerce stylesheet.
 *
 * Removing the default WooCommerce stylesheet and enqueing your own will
 * protect you during WooCommerce core updates.
 *
 * @link https://docs.woocommerce.com/document/disable-the-default-stylesheet/
 */
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

/**
 * Add 'woocommerce-active' class to the body tag.
 *
 * @param  array $classes CSS classes applied to the body tag.
 * @return array $classes modified to include 'woocommerce-active' class.
 */
function hmw_woocommerce_active_body_class( $classes ) {
	$classes[] = 'woocommerce-active';

	return $classes;
}
add_filter( 'body_class', 'hmw_woocommerce_active_body_class' );

/**
 * Products per page.
 *
 * @return integer number of products.
 */
function hmw_woocommerce_products_per_page() {
	return 12;
}
add_filter( 'loop_shop_per_page', 'hmw_woocommerce_products_per_page' );

/**
 * Product gallery thumnbail columns.
 *
 * @return integer number of columns.
 */
function hmw_woocommerce_thumbnail_columns() {
	return 4;
}
add_filter( 'woocommerce_product_thumbnails_columns', 'hmw_woocommerce_thumbnail_columns' );

/**
 * Default loop columns on product archives.
 *
 * @return integer products per row.
 */
function hmw_woocommerce_loop_columns() {
	return 3;
}
add_filter( 'loop_shop_columns', 'hmw_woocommerce_loop_columns' );

/**
 * Related Products Args.
 *
 * @param array $args related products args.
 * @return array $args related products args.
 */
function hmw_woocommerce_related_products_args( $args ) {
	$defaults = array(
		'posts_per_page' => 3,
		'columns'        => 3,
	);

	$args = wp_parse_args( $defaults, $args );

	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'hmw_woocommerce_related_products_args' );

if ( ! function_exists( 'hmw_woocommerce_product_columns_wrapper' ) ) {
	/**
	 * Product columns wrapper.
	 *
	 * @return  void
	 */
	function hmw_woocommerce_product_columns_wrapper() {
		$columns = hmw_woocommerce_loop_columns();
		echo '<div class="columns-' . absint( $columns ) . '">';
	}
}
add_action( 'woocommerce_before_shop_loop', 'hmw_woocommerce_product_columns_wrapper', 40 );


if ( ! function_exists( 'hmw_woocommerce_main_container_wrapper' ) ) {
	/**
	 * Product columns wrapper.
	 *
	 * @return  void
	 */
	function hmw_woocommerce_main_container_wrapper() {
		echo "<section class='container'>";
      echo "<div class='woocommerce-cart__wrapper'>";
	}
}

if ( ! function_exists( 'hmw_woocommerce_main_container_wrapper_close' ) ) {
	/**
	 * Product columns wrapper.
	 *
	 * @return  void
	 */
	function hmw_woocommerce_main_container_wrapper_close() {
      echo "</div>";
    echo "</section>";
    if (function_exists('render_common_globals')) :
      echo render_common_globals();
    endif;
	}
}



if ( ! function_exists( 'hmw_woocommerce_product_columns_wrapper_close' ) ) {
	/**
	 * Product columns wrapper close.
	 *
	 * @return  void
	 */
	function hmw_woocommerce_product_columns_wrapper_close() {
		echo '</div>';
	}
}
add_action( 'woocommerce_after_shop_loop', 'hmw_woocommerce_product_columns_wrapper_close', 40 );

/**
 * Remove default WooCommerce wrapper.
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

if ( ! function_exists( 'hmw_woocommerce_wrapper_before' ) ) {
	/**
	 * Before Content.
	 *
	 * Wraps all WooCommerce content in wrappers which match the theme markup.
	 *
	 * @return void
	 */
	function hmw_woocommerce_wrapper_before() {
		?>
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">
    <div class="container">
      <?php
	}
}
add_action( 'woocommerce_before_main_content', 'hmw_woocommerce_wrapper_before' );

if ( ! function_exists( 'hmw_woocommerce_wrapper_after' ) ) {
	/**
	 * After Content.
	 *
	 * Closes the wrapping divs.
	 *
	 * @return void
	 */
	function hmw_woocommerce_wrapper_after() {
			?>
    </div><!-- .container -->
  </main><!-- #main -->
</div><!-- #primary -->
<?php
	}
}
add_action( 'woocommerce_after_main_content', 'hmw_woocommerce_wrapper_after' );

/**
 * Sample implementation of the WooCommerce Mini Cart.
 *
 * You can add the WooCommerce Mini Cart to header.php like so ...
 *
	<?php
		if ( function_exists( 'hmw_woocommerce_header_cart' ) ) {
			hmw_woocommerce_header_cart();
		}
	?>
*/

if ( ! function_exists( 'hmw_woocommerce_cart_link_fragment' ) ) {
/**
* Cart Fragments.
*
* Ensure cart contents update when products are added to the cart via AJAX.
*
* @param array $fragments Fragments to refresh via AJAX.
* @return array Fragments to refresh via AJAX.
*/
function hmw_woocommerce_cart_link_fragment( $fragments ) {
ob_start();
hmw_woocommerce_cart_link();
$fragments['a.cart-contents'] = ob_get_clean();

return $fragments;
}
}
add_filter( 'woocommerce_add_to_cart_fragments', 'hmw_woocommerce_cart_link_fragment' );

if ( ! function_exists( 'hmw_woocommerce_cart_link' ) ) {
/**
* Cart Link.
*
* Displayed a link to the cart including the number of items present and the cart total.
*
* @return void
*/
function hmw_woocommerce_cart_link() {
?>
<a class="cart-contents text-primary <?php echo WC()->cart->get_cart_contents_count() > 0 ? 'has-items' : '';  ?>"
  href="<?php echo esc_url( wc_get_cart_url() ); ?>"
  title="<?php esc_attr_e( 'View your shopping cart', 'ljharper' ); ?>">
  <?php if (WC()->cart->get_cart_contents_count() > 0) : ?>
  <span class="count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
  <?php endif; ?>
</a>
<?php
	}
}

if ( ! function_exists( 'hmw_woocommerce_header_cart' ) ) {
	/**
	 * Display Header Cart.
	 *
	 * @return void
	 */
	function hmw_woocommerce_header_cart() {
		if ( is_cart() ) {
			$class = 'current-menu-item';
		} else {
			$class = '';
		}
		?>
<ul id="site-headser-cart" class="site-header-cart">
  <li class="<?php echo esc_attr( $class ); ?>">
    <?php hmw_woocommerce_cart_link(); ?>
    <?php
      $instance = array(
        'title' => '',
      );

      the_widget( 'WC_Widget_Cart', $instance );
    ?>
  </li>
</ul>
<?php
	}
}

// DISABLE SHOP
if (function_exists('get_field') && get_field('disable_shop', 'option')) {
  add_action( 'woocommerce_share', function() {
    $reason = get_field('disable_shop_reason', 'option') ?: 'We have temporarily halted new sales';
    echo sprintf('<span class="shop-disabled__reason">%s</span>', $reason);
  }, 20);

  add_filter( 'woocommerce_is_purchasable','__return_false',10,2);
}

/**
 * Override loop template and show "Learn More" button next to add to cart
 */
add_filter( 'woocommerce_loop_add_to_cart_link', function( $html, $product ) {
  if ( $product ) {
    // var_dump($product);
    ob_start(); ?>
<div class="flex flex-wrap cart-buttons">
  <?php 

  if ($product->regular_price !== '') : 
  echo sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="btn bg-secondary text-white %s product_type_%s">%s</a>',
      esc_url( $product->add_to_cart_url() ),
      esc_attr( $product->get_id() ),
      esc_attr( $product->get_sku() ),
      implode( ' ', array_filter( array(
                      'button',
                      'product_type_' . $product->get_type(),
                      $product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
                      $product->supports( 'ajax_add_to_cart' ) ? 'ajax_add_to_cart' : ''
              ) ) ),
      esc_attr( $product->product_type ),
      '<i class="fas text-primary fa-shopping-cart flex mr-1"></i>' .  $product->add_to_cart_text()
      ); 
      endif;
    ?>
  <a href="<?php echo get_the_permalink(get_the_ID()); ?>"
    class="btn text-secondary btn--outlined bg-white border-secondary"><i class="fas fa-info"></i></a>
</div>

<?php
}
return ob_get_clean();
}, 10, 2 );

add_action( 'woocommerce_after_shop_loop_item_title', function() { 
    global $post; 

    //get the individual products' categories and put them in an array
    // $terms = wp_get_post_terms( $post->ID, 'product_cat' );
    // foreach ( $terms as $term ) {
    //     $product_categories[] = $term->term_id;
    // };

    //check if the array contains your specific $category_id that you are targeting
    // if ( is_shop() && in_array( $category_id, $product_categories )) {
        echo $post->post_excerpt ? '<div class="product-loop-excerpt">'.wp_trim_words($post->post_excerpt,10).'</div>' : null;
    // }
}, 5); 


/* Add custom meta */
// The code for displaying WooCommerce Product Custom Fields
add_action( 'woocommerce_product_options_general_product_data', 'woocommerce_product_custom_fields' ); 
// Following code Saves  WooCommerce Product Custom Fields
add_action( 'woocommerce_process_product_meta', 'woocommerce_product_custom_fields_save' );

function woocommerce_product_custom_fields () {
global $woocommerce, $post;
echo '<div class=" product_custom_field ">';
  // Custom Product Text Field
    woocommerce_wp_text_input(
        array(
            'id' => '_product_first_letter',
            'placeholder' => 'First letter of product title',
            'label' => __('First letter of product title', 'woocommerce'),
            'desc_tip' => 'true'
        )
    );
echo '</div>';
}

add_action('save_post_product', 'set_product_letter', 10, 3 );

function set_product_letter($post_id) {

  $oldVal = get_post_meta($post_id, '_product_first_letter');
  $newVal = $_POST["_product_first_letter"];

  // Make sure there is a difference, or is empty before carrying on
  if ($newVal !== $oldVal || !$oldVal) {
    $product = get_product($post_id);
    $name = $product->get_formatted_name();
    update_post_meta($post_id, '_product_first_letter', $name[0]);
  }
  
}

/* Disable coupons field if there are no active coupons in the shop */
function disable_coupon_if_none_exist( $enabled ) {
    
  $args = array(
    'posts_per_page'   => -1,
    'post_type'        => 'shop_coupon',
    'post_status'      => 'publish',
  );
    
  $coupons = get_posts( $args );

  if (empty($coupons)) {
		$enabled = false;
  }
  
	return $enabled;
}
add_filter( 'woocommerce_coupons_enabled', 'disable_coupon_if_none_exist' );


/************************ Single Product ***
 *******************************************/
// Move breadcrumbs into product column
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
add_action('woocommerce_single_product_summary', 'woocommerce_breadcrumb', 1);

/* Remove elements */ 
remove_action( 'woocommerce_before_single_product', 'wc_print_notices', 10 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );

// Remove the "Description" title
add_filter( 'woocommerce_product_description_heading', '__return_null' );

// Move description to underneath price
function woocommerce_template_product_description() {
  ob_start(); ?>
<div class="product-description">
  <?php wc_get_template( 'single-product/tabs/description.php' ); ?>
</div>
<?php
  $res = ob_get_clean();
  echo $res;
}
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_product_description', 20 );


add_filter('woocommerce_product_related_products_heading',function(){
  return __('You may also like', 'plymouth');
});

add_action( 'woocommerce_after_main_content', function() {
  if (is_product() && function_exists('render_common_globals')) :
    echo render_common_globals();
  endif;
}, 30 );

/************************ Cart Page ***
 **************************************/
// add_action('woocommerce_before_cart', 'hmw_woocommerce_main_container_wrapper');
// add_action('woocommerce_after_cart', 'hmw_woocommerce_main_container_wrapper_close');

/********************* Checkout Page ***
 **************************************/
// add_action('woocommerce_before_checkout_form', 'hmw_woocommerce_main_container_wrapper');
// add_action('woocommerce_after_checkout_form', 'hmw_woocommerce_main_container_wrapper_close');

remove_action( 'woocommerce_checkout_order_review', 'woocommerce_order_review', 10 );

add_action( 'woocommerce_checkout_after_order_review', function() {
  echo '<div class="order-review-section">';
    echo '<div class="order-review-table__wrapper">';
      woocommerce_order_review();
    echo '</div>';
      $order_button_text = apply_filters( 'woocommerce_order_button_text', __( 'Place order', 'woocommerce' ) );
      echo '<input type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '" />';
  echo '</div>';
}, 20 );

function remove_woocommerce_order_button_html() {
  return '';
}

add_filter( 'woocommerce_order_button_html', 'remove_woocommerce_order_button_html' );

/**
 * Add fragments for notices
 */
add_filter( 'woocommerce_add_to_cart_fragments', function ( $fragments ) {
    $all_notices  = WC()->session->get( 'wc_notices', array() );
    $notice_types = apply_filters( 'woocommerce_notice_types', array( 'error', 'success', 'notice' ) );

    ob_start();
    foreach ( $notice_types as $notice_type ) {
        if ( wc_notice_count( $notice_type ) > 0 ) {
            wc_get_template( "notices/{$notice_type}.php", array(
                'notices' => array_filter( $all_notices[ $notice_type ] ),
            ) );
        }
    }
    $fragments['notices_html'] = ob_get_clean();

    wc_clear_notices();

    return $fragments;
} );

// Wordpress Ajax: Get product details after adding to cart via ajax
add_action( 'wp_ajax_nopriv_ajax_added_to_cart_items', 'ajax_added_to_cart_items' );
add_action( 'wp_ajax_ajax_added_to_cart_items', 'ajax_added_to_cart_items' );
function ajax_added_to_cart_items() {
    if( isset($_POST['added']) && isset($_POST['product_id']) ){
      $ID = (int) $_POST['product_id'];
      $product = wc_get_product($ID);
        echo json_encode( [
          "product" => $product->get_data(),
          "cart_url" => wc_get_cart_url()
        ] );
    }
 
   die(); // To avoid server error 500
}
