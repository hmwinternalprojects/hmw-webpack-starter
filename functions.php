<?php
/**
 * _s functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package _s
 */

if ( ! function_exists( 'hmw_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function hmw_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on _s, use a find and replace
		 * to change 'hmw' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'hmw', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );


		// Gutenburg
		add_theme_support( 'align-wide' );
		add_theme_support( 'align-full' );

    // Enable excerpts on pages
    add_post_type_support( 'page', 'excerpt' );
    
    // Remove no-js flag for those with JS enabled
    add_action('after_body_open_tag', function() { 
    
      ?>

      <script>
        document.body.classList.remove('no-js');
      </script>

      <?php
    });

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		// Add custom editor css
		add_theme_support( 'editor-styles' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'hmw' ),
			'top-menu' => esc_html__( 'Top Menu', 'hmw' ),
			'footer-menu' => esc_html__( 'Footer Menu', 'hmw' ),
			'footer-menu-bottom' => esc_html__( 'Footer Menu Bottom', 'hmw' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'hmw_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'hmw_setup' );

/* Custom URL Params */
if (!function_exists('hmw_query_vars')) {
  function hmw_query_vars( $qvars ) {
      $qvars[] = 'categories';
      return $qvars;
  }
  add_filter( 'query_vars', 'hmw_query_vars' );
}

/**
 * Set the content width in pixels (gutenburg editor), based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
if (!function_exists('hmw_content_width')) {
  function hmw_content_width() {
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters( 'hmw_content_width', 640 );
  }
  add_action( 'after_setup_theme', 'hmw_content_width', 0 );
}

if (!function_exists('prefix_reset_metabox_positions')) {
  function prefix_reset_metabox_positions(){
    delete_user_meta( wp_get_current_user()->ID, 'meta-box-order_post' );
    delete_user_meta( wp_get_current_user()->ID, 'meta-box-order_page' );
    delete_user_meta( wp_get_current_user()->ID, 'meta-box-order_YOUR_CPT_SLUG' );
  }
  add_action( 'admin_init', 'prefix_reset_metabox_positions' );
}

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function hmw_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'hmw' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'hmw' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'hmw_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function hmw_scripts() {
	// Loads required CSS header only.
	wp_enqueue_style( 'hmw-style', get_stylesheet_uri() );
	wp_enqueue_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js', array(), null, true);
	wp_enqueue_style( 'hmw-child-static-styles', get_stylesheet_directory_uri() . '/static/static.css' );	
  wp_enqueue_script( 'hmw-child-static-defer', get_stylesheet_directory_uri() . '/static/static.js', array('jquery'), null, true );

  // Run scripts through filter
  add_filter('script_loader_tag', 'add_async_defer_to_script', 10, 2);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
  }
  
}
add_action( 'wp_enqueue_scripts', 'hmw_scripts', 9 );

// add async and defer attributes to enqueued scripts
function add_async_defer_to_script($tag, $handle) {
  // Add the word "async" as part of the script handle and it will add the attribute
  if (stripos($handle, 'async')) {
    $tag = str_replace(' src', ' async="async" src', $tag);
  }
  // Add the word "defer" as part of the script handle and it will add the attribute
  if (stripos($handle, 'defer')) {
    $tag = str_replace('<script ', '<script defer ', $tag);
  }
	return $tag;
}

/* Backend CSS */
if (!function_exists('load_custom_wp_admin_style')) {
  function load_custom_wp_admin_style(){
    wp_register_style( 'hmw-theme-options', get_stylesheet_directory_uri() . '/public/hmw-theme-options.css', false, '1.0.0' );
    wp_register_style( 'custom_bmcb_admin_css', get_stylesheet_directory_uri() . '/public/admin.css', false, '1.0.0' );
    wp_enqueue_style( 'custom_bmcb_admin_css' );
    wp_enqueue_style( 'hmw-theme-options' );
  }
  
  add_action('admin_enqueue_scripts', 'load_custom_wp_admin_style');
}

// Add for tinyMCE too
add_editor_style( get_stylesheet_directory_uri() . '/public/hmw-theme-options.css' );
add_editor_style( get_stylesheet_directory_uri() . '/public/admin.css' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
// require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_theme_file_path('/inc/woocommerce.php');
}

/****************************************************************************************************************
* Gravity Forms
*/
// Auto Scroll to Confirmation/Error Message
add_filter( 'gform_confirmation_anchor', '__return_true' );

// Gravity Forms - Hide Label Option
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

// Gravity Forms - Convert the default button markup from <input /> to a <button /> for custom styling. -- Still uses the settings / button text option
add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {
    return "<button class='button gform_button' id='gform_submit_button_{$form['id']}'><span>{$form['button']['text']}</span></button>";
}
function gform_column_splits( $content, $field, $value, $lead_id, $form_id ) {
	if( !IS_ADMIN ) { // only perform on the front end

		// target section breaks
		if( $field['type'] == 'section' ) {
			$form = RGFormsModel::get_form_meta( $form_id, true );

			// check for the presence of multi-column form classes
			$form_class = explode( ' ', $form['cssClass'] );
			$form_class_matches = array_intersect( $form_class, array( 'two-column', 'three-column' ) );

			// check for the presence of section break column classes
			$field_class = explode( ' ', $field['cssClass'] );
			$field_class_matches = array_intersect( $field_class, array('gform_column') );

			// if field is a column break in a multi-column form, perform the list split
			if( !empty( $form_class_matches ) && !empty( $field_class_matches ) ) { // make sure to target only multi-column forms

				// retrieve the form's field list classes for consistency
				$form = RGFormsModel::add_default_properties( $form );
				$description_class = rgar( $form, 'descriptionPlacement' ) == 'above' ? 'description_above' : 'description_below';

				// close current field's li and ul and begin a new list with the same form field list classes
				return '</li></ul><ul class="gform_fields '.$form['labelPlacement'].' '.$description_class.' '.$field['cssClass'].'"><li class="gfield gsection empty">';

			}
		}
	}
	
	return $content;
}
add_filter( 'gform_field_content', 'gform_column_splits', 10, 5 );

/****************************************************************************************************************
* Include Theme Options // used for global fields (e.g social icons)
*/
include get_template_directory() . '/inc/theme_options.php'; 

/****************************************************************************************************************
* Include Custom Shortcodes
*/
include get_theme_file_path( '/inc/shortcodes/default-shortcodes.php' ); // These are the default shortcodes we bundle 
include get_theme_file_path( '/inc/shortcodes/post-shortcodes.php' ); // Everything related to posts
include get_theme_file_path( '/inc/shortcodes/page-shortcodes.php' ); // Everything related to pages


/****************************************************************************************************************
* Include Custom REST API Endpoints
*/
include get_theme_file_path( '/inc/endpoints/loadmore.php' ); // Everything related to pages

/****************************************************************************************************************
 * No Index No Follow Entire Website - Message
 */
if (!function_exists('search_engine_notice')) {
  function search_engine_notice() {
      if( get_option('blog_public') === '0' ){
          $notice_class = 'notice notice-error';
          $notice_message = '<h1 style="color: red; padding-top: 0;" class="blink-2">Search Engine Visibility is disabled!</h1>';
          $notice_message .= 'Please enable it <a href="' . site_url() . '/wp-admin/options-reading.php">here</a>';
          $notice_message .= '<style> .blink-2 { animation: blinker 2s linear infinite; } @keyframes blinker { 50% { opacity: 0; } }</style>';
          $notice_message = __($notice_message);
  
          printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $notice_class ), $notice_message);
  
          unset($notice_class);
          unset($notice_message);
      }
  }
  if( get_option('blog_public') === '0' ) {
      add_action('admin_notices', 'search_engine_notice', 1 );
      add_action( 'wp_head', 'nofollow_meta', 1 );
      add_action( 'login_enqueue_scripts', 'nofollow_meta', 1 );
  }
}

if (!function_exists('nofollow_meta')) {
  function nofollow_meta() {
      echo "<meta name='robots' content='noindex,nofollow' />\n";
  }
}

if (!function_exists('get_theme_colors')) {
  function get_theme_colors() {
    // get the textarea value from options page without any formatting
    // check if the repeater field has rows of data
    $set_colours = [];
    $additional_colours = [];

    if( have_rows('theme_colours', 'option') ) {
        // TEMP -- ACF Update has caused warnings to appear --- The @ symbol will suppress it for now (still works)
        $set_colours_data = get_field('theme_colours', 'option');

        // If it's the old repeater method, just return the repeater /// Backwards compatibility
        if (isset($set_colours_data[0]['name'])) {
            return $set_colours_data;
        }

        $additional_colours_data = array_pop($set_colours_data);

        if (!$additional_colours_data) {
          $additional_colours_data = [];
        }

        foreach($set_colours_data as $key=>$val) {
            $set_arr = [];
            $set_arr['name'] = $key;
            $set_arr['value'] = $val['value'];
            array_push($set_colours, $set_arr);
        }
      return array_merge($set_colours, $additional_colours_data);
    } else {
      return;
    }
  }
}

/**
 * Make ACF use the API key in settings page
 */
function my_acf_init() {
  $API_KEY = get_field('google_api_key', 'option');
	acf_update_setting('google_api_key', $API_KEY); 
}

add_action('acf/init', 'my_acf_init');

/**
 * Enqueue google fonts (if set in site options)
 */
if (!function_exists('options_google_font_enqueue') && function_exists('get_field')) {
  function options_google_font_enqueue() {
    $fonts_enabled = get_field('enable_google_fonts', 'option');
    if (!$fonts_enabled) { return; }
    $subsets = array();
    $font_element = array();
    
    $font_main = get_field('font_main', 'option');
    $font_headings = get_field('font_headings', 'option');
    $font_buttons = get_field('font_buttons', 'option'); 
  
    $font_array = [];
  
    if ($font_main) : 
      if (!is_array($font_array[$font_main['font']])) {
        $font_array[$font_main['font']] = $font_main;
      } else {
        if (is_array($font_main['variants'])) {
          $font_array[$font_main['font']]['variants'] = array_merge($font_array[$font_main['font']]['variants'], $font_main['variants']);
        }
      }
    endif;
    if ($font_headings) :
      if (!is_array($font_array[$font_headings['font']])) {
        $font_array[$font_headings['font']] = $font_headings;
      } else {
        if (is_array($font_headings['variants'])) {
          $font_array[$font_headings['font']]['variants'] = array_merge($font_array[$font_headings['font']]['variants'], $font_headings['variants']);
        }
      }
    endif;
    if ($font_buttons) : 
      if (!is_array($font_array[$font_buttons['font']])) {
        $font_array[$font_buttons['font']] = $font_buttons;
      } else {
        if (is_array($font_buttons['variants'])) {
          $font_array[$font_buttons['font']]['variants'] = array_merge($font_array[$font_buttons['font']]['variants'], $font_buttons['variants']);
        }
      }
    endif;
    if (!empty($font_array)) : 
      foreach($font_array as $key=>$font) :
        if (is_array($font['subsets'] )) {
          $subsets = array_merge( $subsets, $font['subsets'] );
        }
        $font_name = $font['font'];
        if ($font['variants'] == array('regular') || !$font['variants'] ) {
          $font_element[] = $font_name;
        } else {
            if (is_array($font['variants'])) {
              $regular_variant = array_search( 'regular', $font['variants'] );
            }
            if( $regular_variant !== false ) {
                $font['variants'][$regular_variant] = '400';
            }
            $font_element[] = $font_name . ':' . implode( ',', $font['variants'] );
          }
      endforeach;
    endif;
  
    // check if the repeater field has rows of data
    if( get_field('additional_google_fonts', 'option') && have_rows('additional_fonts', 'option') ):
      
      while ( have_rows('additional_fonts', 'option') ) : the_row(); 
        $font = get_sub_field('font_selector');
        if (!$font) { return; }
        if (is_array($font['subsets'] )) {
          $subsets = array_merge( $subsets, $font['subsets'] );
        }
        $font_name = str_replace( ' ', '+', $font['font'] );
        if( $font['variants'] == array( 'regular' ) || !$font['variants'] ) {
            $font_element[] = $font_name;
        }
        else {
          if (is_array($font['variants'])) {
            $regular_variant = array_search( 'regular', $font['variants'] );
          }
          if( $regular_variant !== false ) {
              $font['variants'][$regular_variant] = '400';
          }
          $font_element[] = $font_name . ':' . implode( ',', $font['variants'] );
        }
      endwhile; 
    endif;  
  
    $subsets = ( empty( $subsets ) ) ? array('latin') : array_unique( $subsets );
  
    $subset_string = implode( ',', $subsets );
  
    $font_string = implode( '|', array_unique($font_element));
  
    $request = '//fonts.googleapis.com/css?family=' . $font_string . '&subset=' . $subset_string;

    if ($fonts_enabled) {
      wp_enqueue_style( 'acfgfs-enqueue-fonts', $request );
    }
  }
}

/**
 * Quickly turn a comma string of ID's into an array of integer-type IDs
 */
if (!function_exists('comma_list_to_int_arr')) {
  function comma_list_to_int_arr($string) {
    return array_map(function($id) {
      return (int) trim($id);
    }, explode(',', $string));
  }
}

function format_phone($number, $cc = "61", $html = null, $type = "tel") {
  $phoneURL = preg_replace('/\s+/', '', $number);
  $phoneURL = str_replace("+$cc", '', $phoneURL);
  $phoneURL = str_replace(array( '(', ')' ), '', $phoneURL);
  $phoneURL = ltrim($phoneURL, '0' );
  $formattedPhone = "{$type}:+{$cc}{$phoneURL}";

  if (!$html) :
    return $formattedPhone;
  else : ?>
  <a class="formatted-phone country-code-<?= $cc ?>" href="<?= $formattedPhone; ?>"><?= esc_html__( $number, 'hmw-starter-child' ); ?></a>
<?php 
 endif;
}

// Populate colour options
// Add the global colour options to all of these fields
if (!function_exists('add_site_color_choices')) {

  add_filter('acf/load_field/key=field_5ee954e6b2ab6', 'add_site_color_choices');
  add_filter('acf/load_field/key=field_5eeaef43ae7d2', 'add_site_color_choices');
  add_filter('acf/load_field/key=field_5eeaef41ae7d1', 'add_site_color_choices');
  add_filter('acf/load_field/key=field_5eeb424893807', 'add_site_color_choices');
  
  function add_site_color_choices($field) {
    $field['choices'] = [];
    if (function_exists('get_theme_colors')) : 
      $colours = @get_theme_colors();
      // $colours = [["name" => "red", "value" => "sdf79sf"]];
      if (!empty($colours) && (is_array($colours) || is_object($colours))) :
        foreach($colours as $colour) {
          $colour_name = trim($colour['name']);
          $colour_value = sanitize_hex_color($colour['value']);
          $field['choices'][ 'None' ] = 'None';
          if ($field['ui']) {
            $field['choices'][ $colour_name ] = "<div style='display: flex;'><span style='background: {$colour_value}; width: 20px;
            margin-right: 0.6rem; display: block; border-radius: 50%;'></span>{$colour_name}</div>";
          } else {
            $field['choices'][ $colour_value ] =  $colour_name;
          }
        }
      endif;
      // return the field
    endif; 
    return $field;
  }
}

/* Pagination */
if (function_exists('pagination')) {
  function pagination($pages = '', $current_page = 1, $range = 4)
  {
      $showitems = ($range * 2)+1;
  
      global $paged;
    
    if(empty($paged)) $paged = 1;
  
    // If $pages isn't provided, try get it from global wp_query
      if($pages === '') {
          global $wp_query;
          $pages = $wp_query->max_num_pages;
          if(!$pages) {
              $pages = 1;
          }
      }
  
      if($pages > 1) { 
      ob_start(); ?>

<div class="pagination"><span>
    <?php echo "Page $paged of $pages"; ?></span>

  <?php if($paged > 2 && $paged > $range+1 && $showitems < $pages) : ?>
  <a href='<?php echo get_pagenum_link(1); ?>'>&laquo; First</a>
  <?php endif; ?>

  <?php if($paged > 1 && $showitems < $pages) : ?>
  <a href='<?php echo get_pagenum_link($paged - 1); ?>'>&lsaquo; Previous</a>
  <?php endif; ?>

  <?php for ($i=$current_page; $i <= $pages; $i++) { ?>
  <?php if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) : ?>
  <?php if ($paged == $i) : ?>
  <span class="current"><?php echo $i; ?></span>
  <?php else : ?>
  <a href='<?php echo get_pagenum_link($i); ?>' class="inactive"><?php echo $i; ?></a>
  <?php endif; ?>
  <?php endif; ?>
  <?php } ?>

  <?php if ($paged < $pages && $showitems < $pages) : ?>
  <a href="<?php echo get_pagenum_link($paged + 1); ?>">Next &rsaquo;</a>
  <?php endif; ?>
  <?php if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) : ?>
  <a href='<?php echo get_pagenum_link($pages); ?>'>Last &raquo;</a>
  <?php endif; ?>
</div>

<?php echo ob_get_clean();
      }
  }
}
